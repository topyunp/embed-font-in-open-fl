package;

import starling.core.Starling;
import openfl.display.Sprite;

class Main extends Sprite {
	public function new() {
		super();

		new Starling(MainView, stage).start();
	}
}
