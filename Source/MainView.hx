package;

import openfl.Assets;
import starling.text.TextField;
import starling.display.Sprite;

class MainView extends Sprite {
	public function new() {
		super();

		var font = Assets.getFont("Assets/SourceHanSansCN-ExtraLight.otf");
		trace(font.fontName);

		var tf = new TextField(100, 100);
		tf.format.font = "Source Han Sans CN ExtraLight";
		tf.text = "你好 OpenFL";
		tf.border = true;
		addChild(tf);
	}
}
